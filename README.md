# C Array List

My implementation of an array list in C language.

## Description
Array list is a dynamic data structure used to store data where elements are stored and access through the array.

## Structure
This list is a structure that contains pointer to the data about the list and pointer to the array where elements 
are stored.
```C
typedef struct
{
    void *ldata;
    void *arr;
} ArrayList_t;
```

Stored data is also a structure containing size of data type stored, size of the dynamic array and number of 
elements in that array (in the list).
```C
struct ArrayListData
{
    size_t data_size;
    size_t array_size;
    size_t elements_count;
};
```

Some functions return different error codes, so an enum is provided for easier error handling.
```C
typedef enum {
    AL_OPER_FAIL,      // general error, operation failed
    AL_NO_ELEMS,       // empty list (used when deleting an element)
    AL_IDX_OUT_OF_BDS, // index error
    AL_INSUFF_MEM,     // out of memory; allocation failed
    AL_LIST_TOO_BIG,   // list reached max capacity
    AL_NULLPTR_ERR,    // null pointer error
    AL_OPER_SUCC       // operation successful
} ArrayListReturnCode_t;
```

## Usage
There are 4 main types of functions used to manipulate the list:
#### Adding an element
- `ArrayListReturnCode array_list_insert_first(ArrayList_t *list, const void *element)`
  + Inserts an `element` to the beginning of the `list`
- `ArrayListReturnCode array_list_insert_last(ArrayList_t *list, const void *element)`
  + Inserts an `element` to the end of the `list`
- `ArrayListReturnCode array_list_insert_at(ArrayList_t *list, const void *element, size_t index)`
  + Inserts an `element` to a `list` at position `index`

  All of the above return `AL_OPER_SUCC` if element is added successfully.

#### Removing an element
- `ArrayListReturnCode array_list_remove_first(ArrayList_t *list)`
  + Removes the first element of the `list`
- `ArrayListReturnCode array_list_remove_last(ArrayList_t *list)`
  + Removes the last element of the `list`
- `ArrayListReturnCode array_list_remove_at(ArrayList_t *list, size_t index)`
  + Removes element at position `index` from the `list`

  All of the above return `AL_OPER_SUCC` if element is removed successfully.

#### Getting an element
- `void *array_list_get_first(const ArrayList_t *list)`
  + Returns the first element of a `list` or `NULL` if a list is empty
- `void *array_list_get_last(const ArrayList_t *list)`
  + Returns the last element of a `list` or `NULL` if a list is empty
- `void *array_list_get_at(const ArrayList_t *list, size_t index)`
  + Returns the element at position `index` of a `list` or
  + Returns `NULL` if `position` is invalid or if a `list` is empty
- `size_t array_list_get_element(const ArrayList_t *list, const void *element)`
  + Returns position in a `list` where `element` has first occurred or
  + Returns `SIZE_MAX` and `errno` is set to:
    + `EINVAL` if any of arguments are `NULL`
    + `ENODATA` if element is not present in a list

#### Other functions
- `size_t array_list_get_length(const ArrayList_t *list)`
  + Returns number of elements in a `list` or
  + Returns `SIZE_MAX` and sets `errno` to `EINVAL` if argument is `NULL`
- `ArrayList_t *array_list_create_size(size_t init_capacity, size_t size)`
  + Creates a new array list
  + `capacity` is initial size of a dynamic array. If zero is passed, a default size (20) will be selected
  + `size` is size in bytes of a data type that will be stored in this list
  + Returns a pointer to the list or `NULL` if a list cannot be created
- `array_list_create(size)`
  + Macro that expands to `array_list_create_size(0, size)`
- `void array_list_destroy(ArrayList_t *list)`
  + Delete the `list` and its content from memory
- `ArrayListReturnCode array_list_sort(ArrayList_t *list, int (*comp)(const void *, const void *))`
  + Sorts the `list` while comparing elements using `comp` function
  + Returns `AL_OPER_SUCC` is sorting was successful

## Example
Here is an example showing how the list should be used.
```C
#include "arrayList.h"

int main()
{
    int a;
    ArrayList_t *al = array_list_create(sizeof(int)); // al = []

    if (al == NULL) return 1;

    /* it is assumed that every operation was successful after this point */

    a = 1;
    array_list_insert_last(al, &a); // al = [1]
    a = 2;
    array_list_insert_last(al, &a); // al = [1,2]
    a = 3;
    array_list_insert_first(al, &a); // al = [3,1,2]
    a = 4;
    array_list_insert_at(al, &a, 1); // al = [3,4,1,2]

    a = *(int *) array_list_get_last(al); // a = 2
    a = *(int *) array_list_get_at(al, 2); // a = 1

    array_list_remove_first(al); // al = [4,1,2]
    array_list_remove_at(al, 1); // al = [4,2]

    array_list_destroy(al);
    return 0;
}
```

## License
Everyone is free to use this code as they wish, albeit at their own risk.
