#ifndef ARRAY_LIST_H
#define ARRAY_LIST_H

#include <stdlib.h>
#include <string.h>
#include <errno.h>

typedef struct
{
    void *ldata;
    void *arr;
} ArrayList_t;

typedef enum {
    AL_OPER_FAIL,
    AL_NO_ELEMS,
    AL_IDX_OUT_OF_BDS,
    AL_INSUFF_MEM,
    AL_LIST_TOO_BIG,
    AL_NULLPTR_ERR,
    AL_OPER_SUCC
} ArrayListReturnCode_t;

/**
* @brief Initializes a list with default initial size of the array
*
* @param size size of data type that will be stored in the list
*
* @return pointer to the list if allocation is successful,
*         NULL otherwise
*/
#define array_list_create(size) array_list_create_size(0, size)

ArrayList_t *array_list_create_size(size_t init_capacity, size_t size);
void array_list_destroy(ArrayList_t *list);
ArrayListReturnCode_t array_list_insert_first(ArrayList_t *list, const void *element);
ArrayListReturnCode_t array_list_insert_last(ArrayList_t *list, const void *element);
ArrayListReturnCode_t array_list_insert_at(ArrayList_t *list, const void *element, size_t index);
ArrayListReturnCode_t array_list_remove_first(ArrayList_t *list);
ArrayListReturnCode_t array_list_remove_last(ArrayList_t *list);
ArrayListReturnCode_t array_list_remove_at(ArrayList_t *list, size_t index);
void *array_list_get_first(const ArrayList_t *list);
void *array_list_get_last(const ArrayList_t *list);
void *array_list_get_at(const ArrayList_t *list, size_t index);
size_t array_list_get_element(const ArrayList_t *list, const void *element);
size_t array_list_get_length(const ArrayList_t *list);
ArrayListReturnCode_t array_list_sort(ArrayList_t *list, int (*comp)(const void *, const void *));

#endif // ARRAY_LIST_H
