#include "arrayList.h"

#ifndef SIZE_MAX
#ifdef __SIZE_MAX__
#define SIZE_MAX __SIZE_MAX__
#endif
#endif

#define ARRAY_LIST_DEFAULT_SIZE 20

/* STRUCTS */

/*
  This is internal structure storing data about the list.
  It should not be used on its own.
*/
struct ArrayListData
{
    size_t data_size;
    size_t array_size;
    size_t elements_count;
};


/* MACROS */

#define FREE(a) if(a != NULL) { \
    free(a);                    \
    a = NULL;                   \
    }


/* STATIC FUNCTIONS */

/**
* @brief resize array of the list if its capacity is full.
*     New capacity will be twice the size of the old capacity,
*     unless that size would be greater than SIZE_MAX. In that case,
*     new size will be old_size + default_size
*
* @param *list list that will be resized
*
* @return ArrayListReturnCode_t
*            AL_OPER_SUCC - successful resizing
*            AL_NULLPTR_ERR - null pointer error
*            AL_LIST_TOO_BIG - capacity error
*            AL_INSUFF_MEM - out of memory error
*/
static ArrayListReturnCode_t array_list_resize(ArrayList_t *list)
{
    size_t new_arr_size;
    void *new_arr = NULL;
    struct ArrayListData *ldata = NULL;

    if ((list == NULL) || (list->arr == NULL) || (list->ldata == NULL))
    {
        return AL_NULLPTR_ERR;
    }

    ldata = (struct ArrayListData *) list->ldata;

    // select a new size
    if (ldata->array_size > SIZE_MAX >> 1)
    {
        if (ldata->array_size > SIZE_MAX - ARRAY_LIST_DEFAULT_SIZE)
        {
            return AL_LIST_TOO_BIG;
        }
        new_arr_size = ldata->array_size + ARRAY_LIST_DEFAULT_SIZE;
    }
    else
    {
        new_arr_size = ldata->array_size << 1;
    }

    // ensure that element will fit in the new array
    if (SIZE_MAX / ldata->data_size < new_arr_size)
    {
        return AL_LIST_TOO_BIG;
    }

    new_arr = realloc(list->arr, new_arr_size * ldata->data_size);
    if (new_arr == NULL)
    {
        return AL_INSUFF_MEM;
    }

    list->arr = new_arr;
    ldata->array_size = new_arr_size;

    return AL_OPER_SUCC;
}


/* API FUNCTIONS */

/**
* @brief Initializes a list
*
* @param init_capacity initial size of the array. If argument is
*           zero, the size will be set to ARRAY_LIST_DEFAULT_SIZE
* @param size size of data type that will be stored in the list
*
* @return pointer to the list if allocation is successful,
*         NULL otherwise
*/
ArrayList_t *array_list_create_size(size_t init_capacity, size_t size)
{
    size_t capacity = (init_capacity > 0)
                      ? init_capacity
                      : ARRAY_LIST_DEFAULT_SIZE;
    struct ArrayListData *list_data = NULL;
    void *arr = NULL;
    ArrayList_t *list = NULL;

    if ((size == 0) || (SIZE_MAX / size < capacity))
    {
        return NULL;
    }

    list = (ArrayList_t *) malloc(sizeof(ArrayList_t));
    arr = malloc(capacity * size);
    list_data = (struct ArrayListData *) malloc(sizeof(struct ArrayListData));

    if ((list == NULL) || (arr == NULL) || (list_data == NULL))
    {
        FREE(list);
        FREE(arr);
        FREE(list_data);

        return NULL;
    }

    *list_data = (struct ArrayListData)
    {
        .array_size = capacity,
        .data_size = size,
        .elements_count = 0
    };

    *list = (ArrayList_t)
    {
        .ldata = (void *) list_data,
        .arr = arr
    };

    return list;
}

/**
* @brief Deletes the list and its elements
*
* @param *list pointer to the list
*/
void array_list_destroy(ArrayList_t *list)
{
    if (list != NULL)
    {
        FREE(list->arr);
        FREE(list->ldata);
        FREE(list);
    }
}

/**
* @brief inserts an element to the beginning of the list
*
* @param *list pointer to the list
* @param *element pointer to element whose value will be stored
*
* @return ArrayListReturnCode_t
*            AL_OPER_SUCC - element is added successfully
*            AL_NULLPTR_ERR - null pointer error
*            AL_LIST_TOO_BIG - capacity error
*            AL_INSUFF_MEM - out of memory error
*/
ArrayListReturnCode_t array_list_insert_first(ArrayList_t *list, const void *element)
{
    if ((list == NULL) || (list->arr == NULL)
     || (list->ldata == NULL) || (element == NULL))
    {
        return AL_NULLPTR_ERR;
    }

    return array_list_insert_at(list, element, 0);
}

/**
* @brief inserts an element to the end of the list
*
* @param *list pointer to the list
* @param *element pointer to element whose value will be stored
*
* @return ArrayListReturnCode_t
*            AL_OPER_SUCC - element is added successfully
*            AL_NULLPTR_ERR - null pointer error
*            AL_LIST_TOO_BIG - capacity error
*            AL_INSUFF_MEM - out of memory error
*/
ArrayListReturnCode_t array_list_insert_last(ArrayList_t *list, const void *element)
{
    struct ArrayListData *ldata = NULL;

    if ((list == NULL) || (list->arr == NULL)
     || (list->ldata == NULL) || (element == NULL))
    {
        return AL_NULLPTR_ERR;
    }

    ldata = (struct ArrayListData *) list->ldata;

    return array_list_insert_at(list, element, ldata->elements_count);
}

/**
* @brief inserts an element to the specific position in the list.
*
* @note element is inserted into a dynamic array, meaning that element
*     will not be inserted if it means that an empty space(s) will be
*     left between inserted and last element of the dynamic array, even
*     if the capacity is sufficient enough
*
* @param *list pointer to the list
* @param *element pointer to element whose value will be stored
* @param index position in the list where element will be stored
*
* @return ArrayListReturnCode_t
*            AL_OPER_SUCC - element is added successfully
*            AL_NULLPTR_ERR - null pointer error
*            AL_LIST_TOO_BIG - capacity error
*            AL_INSUFF_MEM - out of memory error
*            AL_IDX_OUT_OF_BDS - index error
*/
ArrayListReturnCode_t array_list_insert_at(ArrayList_t *list, const void *element, size_t index)
{
    int ret_val;
    struct ArrayListData *ldata = NULL;

    if ((list == NULL) || (list->arr == NULL)
     || (list->ldata == NULL) || (element == NULL))
    {
        return AL_NULLPTR_ERR;
    }

    ldata = (struct ArrayListData *) list->ldata;

    if (index > ldata->elements_count)
    {
        return AL_IDX_OUT_OF_BDS;
    }

    // resize array if it's at max capacity
    if (ldata->array_size == ldata->elements_count)
    {
        ret_val = array_list_resize(list);
        if (ret_val != AL_OPER_SUCC) {
            return ret_val;
        }
    }

    // shift elements after a new one for one spot
    memmove((void *) ((unsigned char *) list->arr + (index + 1) * ldata->data_size),
            (void *) ((unsigned char *) list->arr + index * ldata->data_size),
            ldata->data_size * (ldata->elements_count - index));

    // insert an element
    memcpy((void *) ((unsigned char *) list->arr + index * ldata->data_size),
           element,
           ldata->data_size);

    ldata->elements_count++;

    return AL_OPER_SUCC;
}

/**
* @brief removes the first element of the list
*
* @param *list pointer to the list
*
* @return ArrayListReturnCode_t
*            AL_OPER_SUCC - element is removed successfully
*            AL_NULLPTR_ERR - null pointer error
*            AL_NO_ELEMS - no element to remove
*/
ArrayListReturnCode_t array_list_remove_first(ArrayList_t *list)
{
    struct ArrayListData *ldata = NULL;

    if ((list == NULL) || (list->arr == NULL) || (list->ldata == NULL))
    {
        return AL_NULLPTR_ERR;
    }

    ldata = (struct ArrayListData *) list->ldata;

    if (ldata->elements_count == 0)
    {
        return AL_NO_ELEMS;
    }

    return array_list_remove_at(list, 0);
}

/**
* @brief removes the last element of the list
*
* @param *list pointer to the list
*
* @return ArrayListReturnCode_t
*            AL_OPER_SUCC - element is removed successfully
*            AL_NULLPTR_ERR - null pointer error
*            AL_NO_ELEMS - no element to remove
*/
ArrayListReturnCode_t array_list_remove_last(ArrayList_t *list)
{
    struct ArrayListData *ldata = NULL;

    if ((list == NULL) || (list->arr == NULL) || (list->ldata == NULL))
    {
        return AL_NULLPTR_ERR;
    }

    ldata = (struct ArrayListData *) list->ldata;

    if (ldata->elements_count == 0)
    {
        return AL_NO_ELEMS;
    }

    return array_list_remove_at(list, ldata->elements_count - 1);
}

/**
* @brief removes an element from specific position in the list
*
* @param *list pointer to the list
* @param index position in the list where element will be removed from
*
* @return ArrayListReturnCode_t
*            AL_OPER_SUCC - element is removed successfully
*            AL_NULLPTR_ERR - null pointer error
*            AL_NO_ELEMS - no element to remove
*            AL_IDX_OUT_OF_BDS - index error
*/
ArrayListReturnCode_t array_list_remove_at(ArrayList_t *list, size_t index)
{
    struct ArrayListData *ldata = NULL;

    if ((list == NULL) || (list->arr == NULL) || (list->ldata == NULL))
    {
        return AL_NULLPTR_ERR;
    }

    ldata = (struct ArrayListData *) list->ldata;

    if (ldata->elements_count == 0)
    {
        return AL_NO_ELEMS;
    }
    if (index >= ldata->elements_count)
    {
        return AL_IDX_OUT_OF_BDS;
    }

    // shift elements after selected one for one place
    memmove((void *) ((unsigned char *) list->arr + index * ldata->data_size),
            (void *) ((unsigned char *) list->arr + (index + 1) * ldata->data_size),
            (ldata->elements_count - index - 1) * ldata->data_size);

    // delete remaining element after shifting
    memset((void *) ((unsigned char *) list->arr + (ldata->elements_count - 1) * ldata->data_size),
           0,
           ldata->data_size);

    ldata->elements_count--;

    return AL_OPER_SUCC;
}

/**
* @brief get the first element from the list
*
* @param *list pointer to the list
*
* @return element if exists, otherwise NULL
*/
void *array_list_get_first(const ArrayList_t *list)
{
    struct ArrayListData *ldata = NULL;

    if ((list == NULL) || (list->arr == NULL) || (list->ldata == NULL))
    {
        return NULL;
    }

    ldata = (struct ArrayListData *) list->ldata;

    if (ldata->elements_count == 0)
    {
        return NULL;
    }

    return array_list_get_at(list, 0);
}

/**
* @brief get the last element from the list
*
* @param *list pointer to the list
*
* @return element if exists, otherwise NULL
*/
void *array_list_get_last(const ArrayList_t *list)
{
    struct ArrayListData *ldata = NULL;

    if ((list == NULL) || (list->arr == NULL) || (list->ldata == NULL))
    {
        return NULL;
    }

    ldata = (struct ArrayListData *) list->ldata;

    if (ldata->elements_count == 0)
    {
        return NULL;
    }

    return array_list_get_at(list, ldata->elements_count - 1);
}

/**
* @brief get the element from specific position in the list
*
* @param *list pointer to the list
* @param position position of an element in a list
*
* @return element if exists, otherwise NULL
*/
void *array_list_get_at(const ArrayList_t *list, size_t index)
{
    struct ArrayListData *ldata = NULL;

    if ((list == NULL) || (list->arr == NULL) || (list->ldata == NULL))
    {
        return NULL;
    }

    ldata = (struct ArrayListData *) list->ldata;

    if (index >= ldata->elements_count)
    {
        return NULL;
    }

    return (void *) ((unsigned char *) list->arr + index * ldata->data_size);
}

/**
* @brief get the first position of selected element in the list
*
* @param *list pointer to the list
* @param *element element whose value will be checked
*
* @return position of element if exists, otherwise SIZE_MAX and
*         errno is set to a value:
*            EINVAL - any of the arguemnts is NULL
*            ENODATA - element is not present in the list
*/
size_t array_list_get_element(const ArrayList_t *list, const void *element)
{
    size_t index;
    struct ArrayListData *ldata = NULL;

    if ((list == NULL) || (list->arr == NULL)
     || (list->ldata == NULL) || (element == NULL))
    {
        errno = EINVAL;
        return SIZE_MAX;
    }

    ldata = (struct ArrayListData *) list->ldata;

    for (index = 0; index < ldata->elements_count; index++)
    {
        if (memcmp((void *) ((unsigned char *) list->arr + index * ldata->data_size),
                   element,
                   ldata->data_size) == 0)
        {
            return index;
        }
    }

    errno = ENODATA;
    return SIZE_MAX;
}

/**
* @brief get number of elements in the list
*
* @param *list pointer to the list
*
* @return number of element in the list, otherwise SIZE_MAX and
*         errno is set to EINVAL
*/
size_t array_list_get_length(const ArrayList_t *list)
{
    struct ArrayListData *ldata = NULL;

    if ((list == NULL) || (list->arr == NULL) || (list->ldata == NULL))
    {
        errno = EINVAL;
        return SIZE_MAX;
    }

    ldata = (struct ArrayListData *) list->ldata;

    return ldata->elements_count;
}

/**
* @brief sort the list
*
* @param *list pointer to the list
* @param *comp function that will compare elements in the list
*
* @return ArrayListReturnCode_t
*            AL_OPER_SUCC - successful sorting
*            AL_NULLPTR_ERR - null pointer error
*/
ArrayListReturnCode_t array_list_sort(ArrayList_t *list, int (*comp)(const void *, const void *)) {
    struct ArrayListData *ldata = NULL;

    if ((list == NULL) || (list->arr == NULL)
     || (list->ldata == NULL) || (comp == NULL))
    {
        return AL_NULLPTR_ERR;
    }

    ldata = list->ldata;

    qsort(list->arr, ldata->elements_count, ldata->data_size, comp);

    return AL_OPER_SUCC;
}
